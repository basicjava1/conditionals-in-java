package designing;

import java.util.Scanner;

public class MenuDesign {
    public static void main(String[] args) {
        int choice;

        Scanner sc = new Scanner(System.in);
        System.out.println("Enter Number-1: ");
        int number1 = sc.nextInt();

        System.out.println("Enter Number2: ");
        int number2 = sc.nextInt();

        System.out.println("1- Addition");
        System.out.println("2- Subtraction");
        System.out.println("3- Division");
        System.out.println("4- multiplication");
        System.out.println("Select your choice: ");
        choice = sc.nextInt();

        switch (choice)
        {
            case 1:
                System.out.println("Sum is: "+ (number1 + number2));
                break;

            case 2:
                System.out.println("Substraction is: "+ (number1 - number2));
                break;

            case 3:
                System.out.println("Division is: "+ (number1/number2));
                break;

            case 4:
                System.out.println("Multiplication is: " + (number1*number2));
                break;

            default:
                System.out.println("Enter valid choice");
        }
    }
}
